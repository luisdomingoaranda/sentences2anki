{ pkgs ? import <nixpkgs> {} }:
with pkgs;

python3Packages.buildPythonPackage {
  pname = "sentences2anki";
  version = "1.0";

  src = ./.;

  propagatedBuildInputs = with python3Packages; [
    genanki
    google-cloud-texttospeech
  ] ++ [
    anki
  ];

  doCheck = false;
}
