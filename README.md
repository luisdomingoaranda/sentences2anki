# Build language anki cards faster

For each sentence pair `sentence:translation` in a file, get the translation
sound, and create an anki note.

Supported languages:

- Russian - ru
- English - en
- Portuguese - pt

# Usage

1. Create a new file with the following structure

   ```
   $ touch <filename>.<target_language_code>.txt
   ```

   e.g.

   ```
   $ touch sentences.ru.txt
   ```

   There, add as many sentence pairs as you like.

   Lines prefixed with "-" are ignored, so the program won't try to create new
   notes from those.

   ```
   -<deck_name>:<deck_id>
   -
   -Good morning:Доврое утро          # I have already loaded these notes
   -What's your name?:Как вас зовут?  # I have already loaded these notes
   Hi:привет
   Thank you:Спасибо
   ```

   The first line should contain the anki deck name and the deck id
   separated by a colon character.

   If you still don't have a deck, you can put whatever name you like
   and a random 13 digit number as id.

   If you want to add notes to an already existing deck, you need to check its
   name and `deck_id` by inspecting the sqlite database

   ```
   $ sqlitebrowser collection.anki2
   ```

2. Run the script to load the notes into anki, providing you gcloud
   credentials with text-to-speech api enabled, and path to your anki user

   ```
   $ sentences2anki './gcloud_service_account_credentials.json' \
   '/home/username/.local/share/Anki2/User 1' sentences.ru.txt
   ```
