import genanki

MODEL = genanki.Model(
    1627753258975,  # id
    'Languages',  # name
    css=".card {\n font-family: arial;\n font-size: 20px;\n text-align: center;\n color: black;\n background-color: white;\n}\n",
    fields=[
        {'name': "sentence"},
        {'name': "sound"},
        {'name': "sentence_translation"},
        {'name': "extra"},
    ],
    templates=[
        {
          'name': 'Card 1',
          'qfmt': "<div style='font-family: Liberation Sans; font-size: 20px;'>{{sentence}}</div>\n",
          'afmt': "{{FrontSide}}\n\n<hr id=answer>\n\n<div style='font-family: Liberation Sans; font-size: 20px;'>{{sentence_translation}}</div>\n\n{{sound}}\n\n{{extra}}\n\n"
        },
        {
          'name': 'Card 2',
          'qfmt': "<div style='font-family: Liberation Sans; font-size: 20px;'>{{sentence_translation}}</div>\n",
          'afmt': "{{FrontSide}}\n\n<hr id=answer>\n\n<div style='font-family: Liberation Sans; font-size: 20px;'>{{sentence}}</div>\n\n{{sound}}\n\n{{extra}}\n\n"
        },
        {
          'name': 'Card 3',
          'qfmt': "Listening{{sound}}\n",
          'afmt': "{{FrontSide}}\n\n<hr id=answer>\n\n<div style='font-family: Liberation Sans; font-size: 20px;'>{{sentence}}</div>\n\n<br>\n\n<div style='font-family: Liberation Sans; font-size: 20px;'>{{sentence_translation}}</div>\n\n{{extra}}\n\n"
        },
    ]
)
