import sys
import os
import random
import genanki
from anki.storage import Collection
from anki.importing.apkg import AnkiPackageImporter
from google.cloud import texttospeech
from sents2anki.langs import LANGS
from sents2anki.model import MODEL


PATHS = {
    "export_filename": os.path.abspath("exports_tmp.apkg"),
}


def remove_forbidden_media_chars(string):
    # Anki does not allow certain characters present in media filenames
    forbidden_chars = [
        "?",
        ":",
        '"'
    ]

    for forbidden_char in forbidden_chars:
        string = string.replace(forbidden_char, "")

    return string


def get_sound(phrase, language_code):
    # Instantiates a client
    client = texttospeech.TextToSpeechClient()

    # Set the text input to be synthesized
    synthesis_input = texttospeech.SynthesisInput(text=phrase)

    # Build the voice request
    random_voice = random.choice(LANGS[language_code]["voices"])
    voice = texttospeech.VoiceSelectionParams(
        language_code=random_voice["code"],
        name=random_voice["name"]
    )

    # Select the type of audio file you want returned
    audio_config = texttospeech.AudioConfig(
        audio_encoding=texttospeech.AudioEncoding.MP3
    )

    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(
        input=synthesis_input, voice=voice, audio_config=audio_config
    )

    # Write mp3 file
    phrase = remove_forbidden_media_chars(phrase)
    filename = "{}.mp3".format(phrase)
    filepath = "{}/{}".format(PATHS["collection_media_folder_path"], filename)
    with open(filepath, "wb") as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        print('Audio content written to file "{}"'.format(filepath))

    return "[sound:{filename}]".format(filename=filename)


def get_note_fields(line, target_language_code):
    input = line.strip()
    source_sentence = input.split(":")[0]
    sentence_translation = input.split(":")[1]
    sound = get_sound(sentence_translation, target_language_code)

    return [
        source_sentence,
        sound,
        sentence_translation,
        ""  # extra field
    ]


def add_note(deck, model, line, target_language_code):
    my_note = genanki.Note(
        model=model,
        fields=get_note_fields(line, target_language_code)
    )
    deck.add_note(my_note)


def get_inputs():
    # Set environment variable
    credentials_json_path = sys.argv[1]
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials_json_path

    # Set anki paths
    anki_user_path = sys.argv[2]
    PATHS["collection_folder_path"] = anki_user_path + "/collection.anki2"
    PATHS["collection_media_folder_path"] = anki_user_path + "/collection.media"

    # Input file with translations
    input_filename = sys.argv[3]
    target_language_code = input_filename.split(".")[1]

    # Get deck name and id from file header
    deck_name = ""
    deck_id = 0
    with open(input_filename, "r") as lines:
        first_line = lines.readline().replace("-", "").strip()
        deck_name = first_line.split(":")[0]
        deck_id = int(first_line.split(":")[1])

    return input_filename, target_language_code, deck_name, deck_id


def run_main():
    input_filename, target_language_code, deck_name, deck_id = get_inputs()
    my_deck = genanki.Deck(deck_id, deck_name)
    my_model = MODEL

    # Read from file
    with open(input_filename, "r") as lines:
        for line in lines:
            # Ignore lines marked with a "-" prefix
            if (line.startswith("-")):
                continue

            # Add note to deck
            add_note(
                my_deck,
                my_model,
                line,
                target_language_code
            )
            print("Added notes for: {}".format(line))

    # Export notes
    export_filename = PATHS["export_filename"]
    genanki.Package(my_deck).write_to_file(export_filename)
    print("-------------------------------------------------------------------")

    # Import apkg to anki
    collection = Collection(PATHS["collection_folder_path"])
    importer = AnkiPackageImporter(collection, export_filename)
    importer.run()
    print("Notes imported to anki")

    # Remove exported apkg file
    os.remove(PATHS["export_filename"])
