LANGS = {
    # Target languages
    "ru": {
        # Supported voices: https://cloud.google.com/text-to-speech/docs/voices
        "voices": [
            {
                "name": "ru-RU-Wavenet-A",
                "code": "ru-RU"
            },
            {
                "name": "ru-RU-Wavenet-B",
                "code": "ru-RU"
            },
            {
                "name": "ru-RU-Wavenet-C",
                "code": "ru-RU"
            },
            {
                "name": "ru-RU-Wavenet-D",
                "code": "ru-RU"
            },
            {
                "name": "ru-RU-Wavenet-E",
                "code": "ru-RU"
            },
        ]
    },
    "en": {
        # Supported voices: https://cloud.google.com/text-to-speech/docs/voices
        "voices": [
            {
                "name": "en-GB-Wavenet-A",
                "code": "en-GB"
            },
            {
                "name": "en-GB-Wavenet-B",
                "code": "en-GB"
            },
            {
                "name": "en-GB-Wavenet-C",
                "code": "en-GB"
            },
            {
                "name": "en-GB-Wavenet-D",
                "code": "en-GB"
            },
            {
                "name": "en-GB-Wavenet-F",
                "code": "en-GB"
            },
            {
                "name": "en-US-Wavenet-A",
                "code": "en-US"
            },
            {
                "name": "en-US-Wavenet-B",
                "code": "en-US"
            },
            {
                "name": "en-US-Wavenet-C",
                "code": "en-US"
            },
            {
                "name": "en-US-Wavenet-D",
                "code": "en-US"
            },
            {
                "name": "en-US-Wavenet-E",
                "code": "en-US"
            },
        ]
    },
    "pt": {
        # Supported voices: https://cloud.google.com/text-to-speech/docs/voices
        "voices": [
            {
                "name": "pt-BR-Wavenet-A",
                "code": "pt-BR"
            },
            {
                "name": "pt-PT-Wavenet-A",
                "code": "pt-PT"
            },
            {
                "name": "pt-PT-Wavenet-B",
                "code": "pt-PT"
            },
            {
                "name": "pt-PT-Wavenet-C",
                "code": "pt-PT"
            },
            {
                "name": "pt-PT-Wavenet-D",
                "code": "pt-PT"
            },
        ]
    },
}
